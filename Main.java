public class Main {
    public static void main(String[] args) {
        String[] surnames = new String[]{"Khmelnytskyi", "Shuhevych", "Petliura", "Bandera", "Sahaidachny", "Vyshnevetsky","Shevchenko","Mohyla"};
        String[] names = new String[]{"Bohdan", "Roman", "Symon", "Stepan", "Petro", "Dmytro", "Taras", "Petro"};
        InsertionSort.sort(names, surnames);
        for (int i = 0; i < names.length; i++)
            System.out.println(names[i] + " " + surnames[i]);
    }
}

class InsertionSort {
    public static void sort(String[] names, String[] surnames) {
        for (int i = 0; i < names.length; i++) {
            String currElem = names[i];
            String currElem2 = surnames[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && names[prevKey].compareTo(currElem) >= 0) {
                names[prevKey + 1] = names[prevKey];
                names[prevKey] = currElem;
                surnames[prevKey + 1] = surnames[prevKey];
                surnames[prevKey] = currElem2;
                prevKey--;
            }
        }
        for (int i = 0; i < names.length; i++) {
            String currElem = names[i];
            String currElem2 = surnames[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && names[prevKey].equals(currElem) && surnames[prevKey].compareTo(currElem2) > 0) {
                surnames[prevKey + 1] = surnames[prevKey];
                surnames[prevKey] = currElem2;
                prevKey--;
            }
        }
    }
}
